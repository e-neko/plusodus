# plusodus

## architecture:
- end goals:
    - diverse distributed network
        - supernodes: coordination, tunnels, web interface for casual use, data cache and archive
        - home nodes: distributed storage, websockets
            - dht
            - blockchain?
            - public storage/information channels
                  - google photos
                  - pastebin
        - browser extension: distributed storage, websockets, redundancy
    - web clients: casual use, newcomers
    - mobile client

- MVP:
    - supernode
    - web client
    - mobile client

## features:
- friend posts stream
    - bookmarks
    - circles
    - communities
- discoverability
    - friend likes
    - see friend's public friends posts (extended circles)
    - hashtags
    - public communities
    - stretch goals
        - auto-offer similar posts based on machine learning
- privacy
    - per post access (me, unlisted, circle, circles, community, extended, public, announcement(?)
    - per circle access (hide/show circle members to me/unlisted/circle/circles/etc)
    - per account access (public, hide in circles, unlisted, friends only, private)
    - circle levels
        - bookmark (target not notified of following, posts not injected)
        - follow (posts below extended circles not visible)
        - friend (posts in circles visible)
    - sane defaults
- posting
    - site summaries (at least for supported sites, via meta)
        - also in comments
    - upload pictures
        - pictures in comments
    - formatting
        - markdown subset
    - stretch goals
        - upload audio
        - upload video
## stretch goals
  - diaspora compatibility
  - gnusocial compatibility
  - import google+ data (optional)